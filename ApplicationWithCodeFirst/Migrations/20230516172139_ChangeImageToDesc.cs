﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApplicationWithCodeFirst.Migrations
{
    public partial class ChangeImageToDesc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Image",
                table: "categories",
                newName: "Description");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Description",
                table: "categories",
                newName: "Image");
        }
    }
}
