﻿namespace ApplicationWithCodeFirst.Models.EntitiesVM
{
	public class ProductvM
	{
		public string? ProductName { get; set; }
		public string? Description { get; set; }

		public decimal Price { get; set; }
		public string? Image { get; set; }

		public int CategoryId { get; set; }
	}
}
