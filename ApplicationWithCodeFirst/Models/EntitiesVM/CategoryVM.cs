﻿namespace ApplicationWithCodeFirst.Models.EntitiesVM
{
	public class CategoryVM
	{
		public string? CategoryName { get; set; }
		public string? Description { get; set; }
	}
}
