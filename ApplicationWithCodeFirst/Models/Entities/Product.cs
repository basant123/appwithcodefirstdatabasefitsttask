﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationWithCodeFirst.Models.Entities
{
    public class Product
    {
		public int ProductId { get; set; }
		public string? ProductName { get; set; }
		public string? Description { get; set; }

		public decimal Price { get; set; }
		public string? Image { get; set; }

		//public IFormFile File { get; set; } 
		public int CategoryId { get; set; }

		[ForeignKey("CategoryId")]
		public virtual Category Category { get; set; }

	}
}
