﻿using ApplicationWithCodeFirst.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApplicationWithCodeFirst.Models
{
	public class ApplicationDbContext:DbContext
	{

	    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
		{

		}
		public DbSet<Product> products { get; set; }
		public DbSet<Category> categories { get; set; }
	}
}
