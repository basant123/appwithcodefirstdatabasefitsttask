﻿using System.Linq.Expressions;

namespace ApplicationWithCodeFirst.Application.Interface
{
    public interface IRepository<T> where T : class
    {
        // Marks an entity as new
        void Add(T entity);
        // Marks an entity as modified
        void Update(T entity);
        // Marks an entity to be removed
        void Delete(T entity);
        T Get(Expression<Func<T, bool>> filter);
		// Gets all entities of type T
		IEnumerable<T> GetAll();

        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter);


	}
}
