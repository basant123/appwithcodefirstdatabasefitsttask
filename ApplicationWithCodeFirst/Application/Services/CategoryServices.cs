﻿using ApplicationWithCodeFirst.Application.Interface;
using ApplicationWithCodeFirst.Application.Repository;
using ApplicationWithCodeFirst.Models;
using ApplicationWithCodeFirst.Models.Entities;
using System.Linq.Expressions;

namespace ApplicationWithCodeFirst.Application.Services
{
	public class CategoryServices : RepositoryBase<Category>, ICategoryServices
    {
        public CategoryServices(ApplicationDbContext db) : base(db)
        {

        }

        public IEnumerable<Category> GetAllCategories()
        {
            IEnumerable<Category> categories =  GetAll();
            return categories;
        }
    }

	public interface ICategoryServices :IRepository<Category>
	{
        IEnumerable<Category> GetAllCategories();
	}
}
