﻿using ApplicationWithCodeFirst.Application.Interface;
using ApplicationWithCodeFirst.Application.Repository;
using ApplicationWithCodeFirst.Models;
using ApplicationWithCodeFirst.Models.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace ApplicationWithCodeFirst.Application.Services
{
	public class ProductServices : RepositoryBase<Product>, IProductServices
    {
        public ProductServices(ApplicationDbContext db) : base(db)
        {

        }
    }
	public interface IProductServices : IRepository<Product>
	{

	}
}
