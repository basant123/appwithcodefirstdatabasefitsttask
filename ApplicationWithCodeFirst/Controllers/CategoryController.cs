﻿using ApplicationWithCodeFirst.Application.Services;
using ApplicationWithCodeFirst.Models.Entities;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationWithCodeFirst.Controllers
{
	public class CategoryController : Controller
	{
		private readonly ICategoryServices _categoryService;
		public CategoryController(ICategoryServices categoryServices) 
		{ 
		  _categoryService= categoryServices;	
		}	
        public IActionResult Index()
		{

			return View(_categoryService.GetAllCategories()) ;
		}

		public IActionResult Add()
		{

			return View();
		}

		[HttpPost]
		public IActionResult Add(Category category)
		{

			return View();
		}


		public IActionResult Update()
		{

			return View("Add");
		}

		[HttpPost]
		public IActionResult Update(Category category)
		{

			return View();
		}


		public IActionResult Delete()
		{ 
		
			return View();
		}
	}
}
